# Borescope Hardware

<p align="center">
<img src=./Block_Diagram.png width="80%">
</p>

## [M#1 - Servomotors](./M1-Servomotors/README.md)


## [M#2 - Leds](./M2-LEDs/README.md)


## [M#3 - iHead](./M3-iHead/README.md)


## [M#4 - CA Control](./M4-CA Control/README.md)


## [M#5 - Power Management](./M5-Power Management/README.md)


## [M#6 - Battery](./M6-Battery/README.md)


## [M#7 - RPi Hat](./M7-RPi Hat/README.md)


## [M#8 - Keyboard](./M8-Keyboard/README.md)


## [M#9 - Display](./M9-Display/README.md)


## [M#10 - Cooler](./M10-Cooler/README.md)


## [C#x - Connectors](./Cx-Connectors/README.md)

# [Borescope Case](./../3DModels/README.md)