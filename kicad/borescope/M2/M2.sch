EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5550 2800 5550 2700
Wire Wire Line
	5550 2700 4950 2700
Wire Wire Line
	3850 2700 3850 2850
Wire Wire Line
	4400 2850 4400 2700
Connection ~ 4400 2700
Wire Wire Line
	4400 2700 3850 2700
Wire Wire Line
	4950 2850 4950 2700
Connection ~ 4950 2700
Wire Wire Line
	4950 2700 4400 2700
Wire Wire Line
	3850 3200 3850 3250
Wire Wire Line
	3850 3250 4400 3250
Wire Wire Line
	5400 3250 5400 2900
$Comp
L Connector:Conn_01x05_Female J1
U 1 1 61316EE8
P 5750 3000
F 0 "J1" H 5778 3026 50  0000 L CNN
F 1 "Conn_01x05_Female" H 5778 2935 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 5750 3000 50  0001 C CNN
F 3 "~" H 5750 3000 50  0001 C CNN
	1    5750 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 2850 5350 3200
Wire Wire Line
	5550 3200 5350 3200
Wire Wire Line
	4800 3350 5500 3350
Wire Wire Line
	5500 3350 5500 3100
Wire Wire Line
	5500 3100 5550 3100
Wire Wire Line
	5400 2900 5550 2900
Wire Wire Line
	5550 3000 5450 3000
Wire Wire Line
	5450 3000 5450 3300
Wire Wire Line
	5450 3300 4250 3300
Wire Wire Line
	4400 3200 4400 3250
Connection ~ 4400 3250
Wire Wire Line
	4400 3250 4950 3250
Wire Wire Line
	4950 3200 4950 3250
Connection ~ 4950 3250
Wire Wire Line
	4950 3250 5400 3250
$Comp
L Switch:SW_Push UP1
U 1 1 61355599
P 4600 2850
F 0 "UP1" H 4600 3135 50  0000 C CNN
F 1 "SW_Push" H 4600 3044 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm_H4.3mm" H 4600 3050 50  0001 C CNN
F 3 "~" H 4600 3050 50  0001 C CNN
	1    4600 2850
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push BACK1
U 1 1 6135927B
P 4050 2850
F 0 "BACK1" H 4050 3135 50  0000 C CNN
F 1 "SW_Push" H 4050 3044 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm_H4.3mm" H 4050 3043 50  0001 C CNN
F 3 "~" H 4050 3050 50  0001 C CNN
	1    4050 2850
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push HOME1
U 1 1 613599CC
P 5150 2850
F 0 "HOME1" H 5150 3135 50  0000 C CNN
F 1 "SW_Push" H 5150 3044 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm_H4.3mm" H 5150 3050 50  0001 C CNN
F 3 "~" H 5150 3050 50  0001 C CNN
	1    5150 2850
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push DOWN1
U 1 1 6135A671
P 4600 3200
F 0 "DOWN1" H 4600 3485 50  0000 C CNN
F 1 "SW_Push" H 4600 3394 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm_H4.3mm" H 4600 3400 50  0001 C CNN
F 3 "~" H 4600 3400 50  0001 C CNN
	1    4600 3200
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push RIGHT1
U 1 1 6135AC5B
P 5150 3200
F 0 "RIGHT1" H 5150 3485 50  0000 C CNN
F 1 "SW_Push" H 5150 3394 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm_H4.3mm" H 5150 3400 50  0001 C CNN
F 3 "~" H 5150 3400 50  0001 C CNN
	1    5150 3200
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push LEFT1
U 1 1 6135A06B
P 4050 3200
F 0 "LEFT1" H 4050 3485 50  0000 C CNN
F 1 "SW_Push" H 4050 3394 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm_H4.3mm" H 4050 3400 50  0001 C CNN
F 3 "~" H 4050 3400 50  0001 C CNN
	1    4050 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 2850 4800 3200
Wire Wire Line
	4800 3200 4800 3350
Connection ~ 4800 3200
Wire Wire Line
	4250 3300 4250 3200
Wire Wire Line
	4250 3200 4250 2850
Connection ~ 4250 3200
$EndSCHEMATC
