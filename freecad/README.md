# 3D Models

## Borescope
<p align="center">
<img src=./Borescope_3D.png width="80%">
</p>

# Bill of Materials

All screws are M3 and all nuts are ~5mm (5 < width from flats < 5.5)

ref: (quantity) Thread-Length Description of fastened parts

* (4) 5mm Nose-Handle
* (2) 5mm batteries cover
* (1) 15mm left-right handle sides
* (4) 40mm Handle-Raspberry Pi case
* (2) 60mm Handle-Servo holder
* (2) 10mm left-right display case sides
* (2) 5mm keyboard support


# Umbilical Cable

4 conductors flat cable i2c
3 conductors flat cable LEDs PWM signal
2 conductors cable 5V-GND power pair
4 bowden cables
1 USB cable



