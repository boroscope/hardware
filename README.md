# Borescope

The Borescope Project consists of a long probe with a camera and screen that reproduces the augmented image with information about the orientation of the probe. This repository contain the hardware development.

## Requirements

[Software Repository](https://gitlab.com/boroscope/software.git)

## Development

To download this repository, in a terminal type

```
mkdir -p borescope
cd borescope
git clone https://gitlab.com/boroscope/hardware.git
cd hardware
```

In case you want to use **devel** branch, type

```
git checkout devel
```

